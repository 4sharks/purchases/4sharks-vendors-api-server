import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import cookieParser from 'cookie-parser';

import errorHandler from './middleware/errorHandler.js';
import connectDB from './config/dbConnection.js'

import customerRoutes from  './routes/vendorRoutes.js'
import salesmanRoutes from  './routes/salesmanRoutes.js'


dotenv.config();

connectDB();
const app = express();
const port = process.env.PORT || 5000;

// Middlewares
app.use(cors({origin: '*',}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

// Routes

app.use("/vendors",customerRoutes );
app.use("/salesmans",salesmanRoutes );
app.use(errorHandler);


app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});