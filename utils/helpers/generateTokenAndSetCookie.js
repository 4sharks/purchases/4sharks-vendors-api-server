import jwt from 'jsonwebtoken'

const generateTokenAndSetCookie = (userData,res) => {
    const token = jwt.sign(userData, process.env.ACCESS_TOKEN_SECERT,{
        expiresIn: "15d",
    });
    res.cookie("jwt_token", token,{
        httpOnly: true, // more secure
        maxAge: 15 * 24 * 60 * 60 * 1000, // 15 days
        sameSite: "strict", // CSRF
    });

    return token;
};

export default generateTokenAndSetCookie;