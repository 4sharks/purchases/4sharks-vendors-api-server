import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const venddorSchema = mongoose.Schema(
    {
        fullname:[ textWithLang,{required: [true, 'Please enter a name for this customer']}],
        mobileNo: String,
        phoneNo: String,
        email: String,
        vendorNo: String,
        vendorRef: String,
        vendorClassId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'VendorClass',
            default: null,
        },
        vendorGroupsIds:{
            type:[mongoose.Schema.Types.ObjectId],
            ref:'VendorGroup',
            default: null,
        },
        vendorStatusId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'VendorStatus',
            default: null,
        },
        notes: String,
        limitCredit: Number,
        accountNo: String,
        vatNo: String,
        vendorAddresses: [
            {
                name: String,
                city: String,
                region: String,
                street: String,
                desc: String
            }
        ],
        responsibles:[ 
            {
                name:[textWithLang],
                mobileNo: String,
                email: String
            }
        ],
        isActive:{
            type: Boolean,
            default: true,
        },
        tenantId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant',
        },
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant.companies',
        },
        branchId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant.companies.Branches',
        },
    },
    {
        timestamps: true,
    }
);

const Vendor = mongoose.model('Vendor',venddorSchema);

export default Vendor;