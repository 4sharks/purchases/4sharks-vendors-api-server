import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const vendorGroupSchema = mongoose.Schema(
    {
        groupName:[textWithLang, {required:[true,"Please add a Group Name"]}],
        groupDesc:[textWithLang, {required:[true,"Please add a Group Description"]}],
        isActive:{
            type: Boolean,
            default: true
        },
        tenantId: {
            type: String,
            required:[true, 'Please add a tenant id']
        },
        companyId:{
            type: String,
            required:[true, 'Please add a company id']
        },
        branchId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant.companies.Branches',
        },

    },{
        timestamps: true,
    }
);

const vendorGroup = mongoose.model('VendorGroup', vendorGroupSchema);

export default vendorGroup;
