import mongoose from "mongoose";

const textWithLang = {
    text: String,
    lang: String
}

const salesmanSchema = mongoose.Schema(
    {
        fullname:[ textWithLang,{required: [true, 'Please enter a name for this customer']}],
        mobileNo: String,
        email: String,
        salesmanNo: String,
        salesmanRef: String,
        salesmanClassId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'SalesmanClass',
            default: null,
        },
        salesmanStatusId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'SalesmanStatus',
            default: null,
        },
        salesmanNotes: String,
        targetMonthly: Number,
        targetYearly: Number,
        isActive:{
            type: Boolean,
            default: true,
        },
        tenantId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant',
        },
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant.companies',
        },
        branchId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tenant.companies.Branches',
        },
    },
    {
        timestamps: true,
    }
);

const Salesman = mongoose.model('Salesman',salesmanSchema);

export default Salesman