import Vendor from '../models/vendorModel.js'


export const  addNew = async (_data) => {
    try{
        const vendor = await Vendor.create(_data);
        if(!vendor) return false;
        return vendor

    }catch(err){
        console.error(err);
        return false;
    }
}
export const  updateById = async (id,_data) => {
    try{
        const vendor = await Vendor.updateOne({_id:id},_data);
        if(!vendor) return false;
        return vendor

    }catch(err){
        console.error(err);
        return false;
    }
}

export const del = async(id) => {
    const deleted = await Vendor.deleteOne({_id:id});
    if(deleted) return true;
}

export const getByName = async (tenantId,companyId,branchId,fullname) => {
        const customers = await Vendor.find({
                fullname:{
                    $elemMatch: {
                        text : {$regex : new RegExp(fullname,"i") }
                    }
                },
                tenantId : tenantId,
                companyId : companyId,
                branchId : branchId
        })
        if(!customers){
            return false;
        } 
        return customers;
}



export const getAll = async (tenantId,companyId,branchId) => {
    const vendor = await Vendor.find({ 
        tenantId : tenantId,
        companyId : companyId,
        branchId : branchId
    });
        if(vendor) return vendor;
}

export const getById = async (tenantId,companyId,branchId,id) => {
    const vendor = await Vendor.findOne({_id:id}).populate('vendorGroupsIds');
        if(vendor) return vendor;
}