import Salesman from '../models/salesmanModel.js'


export const  addNewSalesman= async (_data) => {
    try{
        const salesman = await Salesman.create(_data);
        if(!salesman) return false;
        return salesman

    }catch(err){
        console.error(err);
        return false;
    }
}
export const  updateSalesmanById = async (id,_data) => {
    try{
        const salesman = await Salesman.updateOne({_id:id},{$set:_data});
        if(!salesman) return false;
        return salesman;
    }catch(err){
        console.error(err);
        return false;
    }
}

export const destroySalesman = async (id) => {
    const deleted = await Salesman.deleteOne({_id:id});
    if(deleted){
        return true;
    }
    return false;
}
export const getSalesman = async (id) => {
    const salesman = await Salesman.findOne({_id:id});
    if(salesman){
        return salesman
    }
    return false;
}
export const getSalesmanByName = async (tenantId,companyId,branchId,salesmanName) => {
        const salesman = await Salesman.find({
                fullname:{
                    $elemMatch: {
                        text : {$regex : new RegExp(salesmanName,"i") }
                    }
                },
                tenantId : tenantId,
                companyId : companyId,
                branchId : branchId
        })
        if(!salesman){
            return false;
        } 
        return salesman;
}

export const getAllSalesman = async (tenantId,companyId,branchId) => {
    const salesman = await Salesman.find({
        tenantId : tenantId,
        companyId : companyId,
        branchId : branchId
    })
        if(salesman) return salesman;
}
