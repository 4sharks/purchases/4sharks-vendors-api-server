import { addNew,updateById,del,getByName,getAll,getById } from "../services/vendorService.js";


export const add = async (req, res) => {
    const {
        fullname,
        mobileNo, 
        vendorNo, 
        vendorRef,
        vendorClassId,
        vendorStatusId,
        notes,
        phoneNo,
        limitCredit,
        accountNo,
        vatNo,
        vendorGroupsIds,
        responsibles,
        vendorAddresses,
        isActive,
        tenantId,
        companyId,
        branchId,
        } = req.body ;
    const _data =    {
        fullname,
        mobileNo, 
        vendorNo, 
        vendorRef,
        vendorClassId,
        vendorStatusId,
        notes,
        phoneNo,
        limitCredit,
        accountNo,
        vatNo,
        vendorGroupsIds,
        responsibles,
        vendorAddresses,
        isActive,
        tenantId,
        companyId,
        branchId,
    } 
    if(!fullname){
        res.status(400).json({status:0,message:'please enter fullname'});
        // throw new Error("Fullname field is mandatory!");
    }
    const add = await addNew(_data);
    if(add){
        return res.status(200).json({status:1,message:'Success',data:add})
    }else{
        return res.status(404).json({status:0,message:'Error'})
    }
}
export const update = async (req, res) => {
    const id = req.params.id;
    const {
        fullname,
        mobileNo, 
        vendorNo, 
        vendorRef,
        vendorClassId,
        vendorStatusId,
        notes,
        phoneNo,
        limitCredit,
        accountNo,
        vatNo,
        vendorGroupsIds,
        responsibles,
        vendorAddresses,
        isActive,
        tenantId,
        companyId,
        branchId,
        } = req.body ;
    const _data =    {
        fullname,
        mobileNo, 
        vendorNo, 
        vendorRef,
        vendorClassId,
        vendorStatusId,
        notes,
        phoneNo,
        limitCredit,
        accountNo,
        vatNo,
        vendorGroupsIds,
        responsibles,
        vendorAddresses,
        isActive,
        tenantId,
        companyId,
        branchId,
    } 
    if(!fullname){
        return res.status(400).json({status:0,message:'please enter fullname'});
        // throw new Error("Fullname field is mandatory!");
    }
    const add = await updateById(id,_data);
    if(add){
        return res.status(200).json({status:1,message:'Success',data:add})
    }else{
        return res.status(404).json({status:0,message:'Error'})
    }
}

export const deleteById = async(req, res) => {
    const id = req.params.id
    const deleted = await del(id);
    if(deleted) return res.status(200).json({status:1,message:'Deleted'})
    else return res.status(404).json({status:0,message:'Error'});
}

export const getVendorByName = async (req,res) => {
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId
    const fullname = req.params.fullname

    const vendors =  await getByName(tenantId,companyId,branchId,fullname)
    if(!vendors) return res.status(401).json({status:0,message:'error',data:vendors})
    if(vendors) return res.status(200).json({status:1,message:'success',data:vendors})
}

export const getAllVendors = async (req,res) => {
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId
    const customers =  await getAll(tenantId,companyId,branchId)
    if(customers) return res.status(200).json({status:1,message:'success',data:customers})
}

export const getVendor = async (req,res) => {
    
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId
    const id = req.params.id
    const customers =  await getById(tenantId,companyId,branchId,id)
    if(customers) return res.status(200).json({status:1,message:'success',data:customers})
}