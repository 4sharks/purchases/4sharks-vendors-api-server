import VendorGroup from "../models/vendorGroupModel.js";

const addGroup = async (req,res) =>{
    const {
        groupName,
        groupDesc,
        isActive,
        tenantId,
        companyId,
        branchId
    } = req.body;

    if(!groupName || groupName.length === 0) return res.status(404).json({status:0,message:'invalid group name'});
    
    const added = await VendorGroup.create({
        groupName,
        groupDesc,
        isActive,
        tenantId,
        companyId,
        branchId
    });

    if(added) return res.status(200).json({status:1,message:'added successfully',data:added});
    else return res.status(404).json({status:0,message:'Error creating customer group'});

}

const updateGroup = async (req,res) =>{
    const id = req.params.id;
    const {
        groupName,
        groupDesc,
        isActive,
        tenantId,
        companyId,
        branchId
    } = req.body;

    if(!groupName || groupName.length === 0) return res.status(404).json({status:0,message:'invalid group name'});
    
    const updated = await VendorGroup.updateOne(
        {_id:id},
        {
            $set:{
                    groupName,
                    groupDesc,
                    isActive,
                    tenantId,
                    companyId,
                    branchId
            }
        });

    if(updated) return res.status(200).json({status:1,message:'updated successfully',data:updated});
    else return res.status(404).json({status:0,message:'Error update customer group'});

}

const getGroup = async(req,res) => {

    const {tenantId,companyId,branchId} = req.params;

    const fetchData = await VendorGroup.find({
        tenantId,companyId,branchId
    }).sort({createdAt:-1}).limit(50);

    if(fetchData) return res.status(200).json({status:1 ,message:'Customer group',data:fetchData});
    else return res.status(404).json({status:0,message:'Error fetch customer group'});
}
const getGroupById = async(req,res) => {

    const {id} = req.params;

    const fetchData = await VendorGroup.findOne({
        _id:id,
    });

    if(fetchData) return res.status(200).json({status:1 ,message:'Vendor group Added',data:fetchData});
    else return res.status(404).json({status:0,message:'Error fetch customer group'});
}

const deleteGroup = async(req,res) => {

    const id = req.params.id;

    const deleted = await VendorGroup.deleteOne({_id:id});

    if(deleted) 
        return res.status(204).json({status:1,message:'Vendor group deleted',data:deleted});
    else 
        return res.status(404).json({status:0,message:'Error during delete group'});
}

export {addGroup,updateGroup,getGroup,getGroupById,deleteGroup}