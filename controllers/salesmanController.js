import { addNewSalesman,updateSalesmanById,getSalesman,destroySalesman,getSalesmanByName,getAllSalesman } from "../services/salesmanService.js";


export const addSalesman = async (req, res) => {
    const {
            fullname,
            mobileNo, 
            email,
            salesmanNo, 
            salesmanRef,
            salesmanClassId,
            salesmanStatusId,
            salesmanNotes,
            targetMonthly,
            targetYearly,
            isActive,
            tenantId,
            companyId,
            branchId,
        } = req.body ;
    const _data =    {
        fullname,
        mobileNo, 
        email,
        salesmanNo, 
        salesmanRef,
        salesmanClassId,
        salesmanStatusId,
        salesmanNotes,
        targetMonthly,
        targetYearly,
        isActive,
        tenantId,
        companyId,
        branchId,
    } 
    if(!fullname){
        res.status(400).json({status:0,message:'please enter fullname'});
    }
    const add = await addNewSalesman(_data);
    if(add){
        res.status(200).json({status:1,message:'Success',data:add})
    }else{
        res.status(404).json({status:0,message:'Error'})
    }
}

export const updateSalesman = async (req, res) => {
    const id = req.params.id
    const {
            fullname,
            mobileNo, 
            email,
            salesmanNo, 
            salesmanRef,
            salesmanClassId,
            salesmanStatusId,
            salesmanNotes,
            targetMonthly,
            targetYearly,
            isActive,
            tenantId,
            companyId,
            branchId,
        } = req.body ;
    const _data =    {
        fullname,
        mobileNo, 
        email,
        salesmanNo, 
        salesmanRef,
        salesmanClassId,
        salesmanStatusId,
        salesmanNotes,
        targetMonthly,
        targetYearly,
        isActive,
        tenantId,
        companyId,
        branchId,
    } 
    if(!fullname){
        res.status(400).json({status:0,message:'please enter fullname'});
    }
    const updated = await updateSalesmanById(id,_data);
    if(updated){
        res.status(200).json({status:1,message:'Success',data:updated})
    }else{
        res.status(404).json({status:0,message:'Error'})
    }
}

export const deleteSalesman = async (req, res) => {
    const id = req.params.id;
    const deleted = await destroySalesman(id);
    if(deleted){
        return res.status(200).json({status:1,message:'deleted'});
    }
}

export const getSalesmanById = async (req,res) => {
    const id = req.params.id;
    const salesman = await getSalesman(id);
    if(salesman){
        return res.status(200).json({status:1,message:'success',data:salesman});
    }
    return res.status(404).json({status:0,message:'error'});
}

export const getByName = async (req,res) => {
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId
    const fullname = req.params.fullname
    const salesman =  await getSalesmanByName(tenantId,companyId,branchId,fullname)
    if(!salesman) res.status(200).json({status:0,message:'error',data:salesman})
    if(salesman) res.status(200).json({status:1,message:'success',data:salesman})
}

export const getAll = async (req,res) => {
    const tenantId = req.params.tenantId
    const companyId = req.params.companyId
    const branchId = req.params.branchId
    const salesman =  await getAllSalesman(tenantId,companyId,branchId)
    if(salesman) res.status(200).json({status:1,message:'success',data:salesman})
}