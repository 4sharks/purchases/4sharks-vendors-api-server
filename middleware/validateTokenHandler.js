import jwt from 'jsonwebtoken'

const validateToken = (req, res, next) => {
        let token;
        //console.log('jwt:',req.cookies.jwt_token);
        let authHeader = req.headers.Authorization || req.headers.authorization;
        if (authHeader && authHeader.startsWith("Bearer")) {
            token = authHeader.split(" ")[1];
        }else if (req.cookies.jwt_token){
            token = req.cookies.jwt_token
        }
        if (!token) {
            return res.status(401).json({
                status:0,
                message:'User is not authorized or token is missing',
                data:[]
            });
            //throw new Error("User is not authorized or token is missing");
        }

        jwt.verify(token, process.env.ACCESS_TOKEN_SECERT, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                status:0,
                message:'User is not authorized',
                data:[]
            });
        }
        req.user = decoded;
        next();
        });
    }

export default validateToken;