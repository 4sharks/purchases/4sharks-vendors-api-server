import express from "express";
import {addSalesman,updateSalesman,deleteSalesman,getSalesmanById,getByName,getAll} from '../controllers/salesmanController.js'

const router = express.Router();

router.post('/',addSalesman);
router.put('/update/:id',updateSalesman);
router.delete('/delete/:id',deleteSalesman);
router.get('/show/:id',getSalesmanById);
router.get('/:tenantId/:companyId/:branchId/',getAll);
router.get('/name/:tenantId/:companyId/:branchId/:fullname',getByName);

export default router;