import express from "express";
import {add,update,deleteById,getVendorByName,getAllVendors,getVendor} from '../controllers/vendorController.js'
import {addGroup,updateGroup,getGroup,getGroupById,deleteGroup} from '../controllers/vendorGroupController.js'

const router = express.Router();

// Vendor Groups routes
router.post('/groups',addGroup);
router.put('/groups/update/:id',updateGroup);
router.delete('/groups/delete/:id',deleteGroup);
router.get('/groups/show/:id',getGroupById);
router.get('/groups/:tenantId/:companyId/:branchId',getGroup);

// Vendor routes
router.post('/',add);
router.delete('/delete/:id',deleteById);
router.put('/update/:id',update);
router.get('/name/:tenantId/:companyId/:branchId/:fullname',getVendorByName);
router.get('/show/:tenantId/:companyId/:branchId/:id',getVendor);
router.get('/:tenantId/:companyId/:branchId/',getAllVendors);



export default router;